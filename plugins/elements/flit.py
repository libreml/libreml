from buildstream import BuildElement


# Element implementation for the 'flit' kind.
class FlitElement(BuildElement):
    pass


# Plugin entry point
def setup():
    return FlitElement

